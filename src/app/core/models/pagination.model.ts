export interface IPagination {
  last?: string;
  next?: string;
  self?: string;
}

interface IHrefContainer {
  href: string;
}

export interface IPaginationBDM {
  last?: IHrefContainer,
  next?: IHrefContainer,
  self?: IHrefContainer,
}
