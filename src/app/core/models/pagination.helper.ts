import {IPagination, IPaginationBDM} from "./pagination.model";

export class PaginationHelper {
  static paginationBDMToModelTransformer(apiDataModel: IPaginationBDM): IPagination {
    return {
      last: apiDataModel.last.href,
      next: apiDataModel.next.href,
      self: apiDataModel.self.href
    } as IPagination;
  }
}
