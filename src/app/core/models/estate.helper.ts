import {IEstateAttributesBDM, IEstateAttributes} from "./estate.model";

export class EstateHelper {
  static estateAttributesBDMToModelTransformer(apiDataModel: IEstateAttributesBDM): IEstateAttributes {
    return {
      location: apiDataModel.field_inmu_pobl,
      rooms: apiDataModel.field_inmu_nume_habi,
      streetName: apiDataModel.field_inmu_nomb_call,
      streetType: apiDataModel.field_inmu_tipo_via,
      price: apiDataModel.field_inmu_prec,
      images: apiDataModel.field_inmu_imag_arra,
      reference: apiDataModel.field_inmu_refe,
      type: apiDataModel.field_inmu_tipo_sin_agru,
      buildArea: apiDataModel.field_inmu_area_cons,
    } as IEstateAttributes;
  }
}
