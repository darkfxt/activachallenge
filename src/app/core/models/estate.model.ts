export interface IEstateAttributes {
  buildArea?: string;
  images?: Array<string>;
  streetName?: string;
  rooms?: number;
  location?: string;
  price?: string;
  reference?: string;
  streetType?: string;
  type?: string;
}

interface IEstateBase {
  type: string;
  id: string;
  links: any;
}

export interface IEstate extends IEstateBase {
  attributes: IEstateAttributes;
}

export interface IEstateBDM extends IEstateBase {
  attributes: IEstateAttributesBDM
}

export interface IEstateAttributesBDM {
  field_inmu_area_cons: string;
  field_inmu_imag_arra: Array<string>;
  field_inmu_nomb_call: string;
  field_inmu_nume_habi: number;
  field_inmu_pobl: string;
  field_inmu_prec: string;
  field_inmu_refe: string;
  field_inmu_tipo_sin_agru: string;
  field_inmu_tipo_via: string;
}
