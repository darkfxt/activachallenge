import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'estate',
    pathMatch: 'full',
  },
  {
    path: 'estate',
    loadChildren: () =>
      import('./features/estate-list/estate-list-routing.module').then((m) => m.EstateListRoutingModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
