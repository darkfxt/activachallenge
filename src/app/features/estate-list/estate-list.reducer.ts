import {IEstate} from "../../core/models/estate.model";
import {IPagination} from "../../core/models/pagination.model";
import {Action, createReducer, on} from "@ngrx/store";
import * as estateListActions from './estate-list.actions';

export interface IEstateState {
  items: Array<IEstate>;
  pagination: IPagination;
  loading: boolean;
}

export const initialState: IEstateState = {
  items: [],
  pagination: {},
  loading: false,
};

const estatesReducer = createReducer(
  initialState,
  on(estateListActions.loadEstatePaginatedList, (state, { pagination }) => ({ ...state, pagination, loading: true})),
  on(estateListActions.loadedEstateListSuccesfully, (state, { items, pagination }) => ({ ...state, items, pagination, loading: false})),
);

export function estateReducer(state: IEstateState | undefined, action: Action): IEstateState {
  return estatesReducer(state, action);
}

export const estateFeatureKey = 'Estate';
