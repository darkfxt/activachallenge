import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Observable, Subject} from "rxjs/index";
import {select, Store} from "@ngrx/store";
import {selectEstateItemsList, selectEstateLoading, selectEstatePagination} from "../estate-list.selectors";
import {loadEstateList, loadEstatePaginatedList} from "../estate-list.actions";
import {IPagination} from "../../../core/models/pagination.model";
import {BreakpointObserver, Breakpoints, BreakpointState} from "@angular/cdk/layout";

@Component({
  selector: 'activa-estate-list-component',
  templateUrl: './estate-list.component.html',
  styleUrls: ['./estate-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class EstateListComponent implements OnInit, OnDestroy {
  public items$: Observable<any> = this.store.pipe(select(selectEstateItemsList));
  public pagination$: Observable<any> = this.store.pipe(select(selectEstatePagination));
  public loader$: Observable<boolean> = this.store.pipe(select(selectEstateLoading));
  public columnsNumber;
  private destroy$: Subject<any>;
  private pagination: IPagination;
  constructor(
    private store: Store,
    private breakpointObserver: BreakpointObserver,
  ) {
    breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
      Breakpoints.Medium,
      Breakpoints.Large,
      Breakpoints.XLarge
    ])
      .subscribe({
        next: (state: BreakpointState) => {
          if (state.breakpoints[Breakpoints.XSmall]) {
            this.columnsNumber = 1;
          }
          if (state.breakpoints[Breakpoints.Small]) {
            this.columnsNumber = 2;
          }
          if (state.breakpoints[Breakpoints.Medium]) {
            this.columnsNumber = 3;
          }
          if (state.breakpoints[Breakpoints.Large] || state.breakpoints[Breakpoints.XLarge]) {
            this.columnsNumber = 4;
          }
        }
      })
  }

  ngOnInit() {
    this.store.dispatch(loadEstateList());
    this.store.subscribe({ next: (state: any) => {
      this.pagination = state.Estate.pagination;
    }})
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

  onLoadMore() {
    this.store.dispatch(loadEstatePaginatedList({pagination: this.pagination}));
  }
}
