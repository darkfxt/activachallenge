import {Component, Input, OnInit, ViewEncapsulation} from "@angular/core";
import {IEstateAttributes} from "../../../../core/models/estate.model";

@Component({
  selector: 'activa-estate-summarized-card-component',
  templateUrl: './estate-summarized-card.component.html',
  styleUrls: ['./estate-summarized-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EstateSummarizedCardComponent implements OnInit {
  @Input() attributes: IEstateAttributes;

  ngOnInit(): void {
  }

}
