import { NgModule } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import {EstateListComponent} from "./components/estate-list.component";

const routes: Routes = [
  {
    path: '',
    component: EstateListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EstateListRoutingModule {}
