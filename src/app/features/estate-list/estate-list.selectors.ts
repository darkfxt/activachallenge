import {createFeatureSelector, createSelector} from "@ngrx/store";
import {estateFeatureKey, IEstateState} from "./estate-list.reducer";
import {AppState} from "../../core/core.state";

export interface State extends AppState {
  Estate: IEstateState,
}

export const selectEstateFeatureState = createFeatureSelector<State ,IEstateState>(estateFeatureKey);
export const selectEstateItemsList = createSelector(selectEstateFeatureState,
  (state: IEstateState) => state.items
);
export const selectEstatePagination = createSelector(selectEstateFeatureState,
  (state: IEstateState) => state.pagination
);
export const selectEstateLoading = createSelector(selectEstateFeatureState,
  (state: IEstateState) => state.loading
);
