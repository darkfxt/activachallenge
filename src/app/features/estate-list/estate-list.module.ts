import {NgModule} from '@angular/core';
import {EstateListComponent} from "./components/estate-list.component";
import {EstateSummarizedCardComponent} from "./components/estate-summarized-card/estate-summarized-card.component";
import {CommonModule} from "@angular/common";
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {EstateListEffects} from "./estate-list.effects";
import {estateFeatureKey, estateReducer} from "./estate-list.reducer";
import {EstateListMaterialModule} from "./estate-list-material.module";

const COMPONENTS = [
  EstateListComponent,
  EstateSummarizedCardComponent,
];

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(estateFeatureKey, estateReducer),
    EffectsModule.forFeature([EstateListEffects]),
    EstateListMaterialModule,
  ],
  exports: [],
  declarations: [COMPONENTS],
  providers: [],
})
export class EstateListModule {}
