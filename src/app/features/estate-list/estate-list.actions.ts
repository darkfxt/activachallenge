import {createAction, props} from "@ngrx/store";
import {IPagination} from "../../core/models/pagination.model";
import {IEstate} from "../../core/models/estate.model";

export const ACTION_GET_ESTATE_LIST = '[Estate] Load list of estate';
export const loadEstateList = createAction(ACTION_GET_ESTATE_LIST);

export const ACTION_GET_ESTATE_PAGINATED_LIST = '[Estate] Load paginated list of estate';
export const loadEstatePaginatedList = createAction(
  ACTION_GET_ESTATE_PAGINATED_LIST,
  props<{pagination?: IPagination}>()
);

export const ACTION_ESTATE_LIST_LOADED_SUCCESFULLY = '[Estate] List Loaded Succesfully';
export const loadedEstateListSuccesfully = createAction(
  ACTION_ESTATE_LIST_LOADED_SUCCESFULLY,
  props<{items: Array<IEstate>, pagination?: IPagination}>(),
);

export const ACTION_ESTATE_LIST_LOADED_ERROR = '[Estate] List Loaded Error';
export const loadedEstateListError = createAction(ACTION_ESTATE_LIST_LOADED_ERROR);
