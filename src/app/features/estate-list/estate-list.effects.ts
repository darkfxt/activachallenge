import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import {catchError, map, mergeMap, withLatestFrom} from 'rxjs/internal/operators';
import * as fromEstate from './estate-list.actions';
import {EstateListService} from "./estate-list.service";
import {IEstate, IEstateBDM} from "../../core/models/estate.model";
import {EstateHelper} from "../../core/models/estate.helper";
import {PaginationHelper} from "../../core/models/pagination.helper";
import {IPaginationBDM} from "../../core/models/pagination.model";
import {AppState} from "../../core/core.state";
import {Store} from "@ngrx/store";

@Injectable()
export class EstateListEffects {

  constructor(private actions$: Actions, private estateService: EstateListService, private store$: Store<AppState>) {}

  public loadItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromEstate.ACTION_GET_ESTATE_LIST),
      mergeMap(() =>
        this.estateService.getAll().pipe(
          map((items) =>
            fromEstate.loadedEstateListSuccesfully({ items: (items.data.map((estate: IEstateBDM) => {
              return {
                ...estate,
                attributes: EstateHelper.estateAttributesBDMToModelTransformer(estate.attributes),
              }
            })) as IEstate[],
            pagination: PaginationHelper.paginationBDMToModelTransformer(items.links as IPaginationBDM)
            })),
          catchError(() => of({ type: fromEstate.ACTION_ESTATE_LIST_LOADED_ERROR })),
        ),
      ),
    ),
  );

  public loadMore$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromEstate.ACTION_GET_ESTATE_PAGINATED_LIST),
      withLatestFrom(this.store$),
      mergeMap((payload: any) => {
        const loadedItems = payload[1].Estate.items || [];
        return this.estateService.loadMore(payload[0].pagination.next).pipe(
          map((items) =>
            fromEstate.loadedEstateListSuccesfully({ items: [...loadedItems,
                ...(items.data.map((estate: IEstateBDM) => {
                return {
                  ...estate,
                  attributes: EstateHelper.estateAttributesBDMToModelTransformer(estate.attributes),
                }
              })) as IEstate[]
              ],
              pagination: PaginationHelper.paginationBDMToModelTransformer(items.links as IPaginationBDM)
            })),
          catchError(() => of({ type: fromEstate.ACTION_ESTATE_LIST_LOADED_ERROR })),
        )},
      ),
    ),
  );
}
