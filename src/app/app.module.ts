import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app/app.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {CommonModule} from "@angular/common";
import {CoreModule} from "./core/core.module";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatDialogModule} from "@angular/material/dialog";
import {EstateListModule} from "./features/estate-list/estate-list.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    /* Angular */
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    CommonModule,

    /* Core */
    CoreModule,

    /* App */
    AppRoutingModule,
    EstateListModule,

    /* Material */
    MatToolbarModule,
    MatIconModule,
    MatDialogModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
